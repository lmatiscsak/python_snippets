from requests import Request, Session
from getpass import getuser
from os import makedirs

#ADName = input("Enter Your AD Name (no domain): ")
#ADPassword = input("Enter Your Password: ")
#AWSAccountNumber = input("Enter Your AWS Account Number: ") 
#AWSRole = input("Enter Your AWS Role: ") 
#AWSAccountNumber = '969859503720' 
#AWSRole = 'UENTAWSCLOUDADMINS'
ADName = 'lmatiscsak'
ADPassword = ''
AWSAccountNumber = '933851626267' 
AWSRole = 'UENTAWSCCDEVSECOPS'

##Requesting Access and Secret Key
s = Session()
url = "https://awsapi.us.aegon.com/default.aspx"
payload = {"method":"usernameroleaccount","username":ADName,"password":ADPassword,"account":AWSAccountNumber,"role":AWSRole}
req = Request('POST', url, data=payload)
prepped = req.prepare()

resp = s.send(prepped,
    stream=True,
    verify=False
)

print("Here it gives a warning when you make the request. Just ignore, it won't stop the script.")

creds = resp.content.decode().split(',')
AccessKey = creds[2]
SecretAccessKey = creds[0]
SessionToken = creds[1]

print("Access Key: " + AccessKey)
print("Secret Access Key: " + SecretAccessKey)
print("Session Token: " + SessionToken)

##Create a config file for AWS
file_list = 'C:/Users/' + getuser() + '/.aws'
try:
    makedirs(file_list)
    print('Folder: ', file_list, ' has been created!' )
except:
    print('An error occured while creating folder: ', file_list, ' ,it may already exist!')

aws_config = 'C:/Users/' + getuser() + '/.aws/config'
fh = open(aws_config,'w')
fh.write('[default]' + '\n' + 'output = text' + '\n' + 'region = eu-west-1')
fh.close()

aws_cred = 'C:/Users/' + getuser() + '/.aws/credentials'
fh = open(aws_cred,'w')
fh.write('[default]' + '\n' + 'aws_access_key_id = ' + AccessKey + '\n' + 'aws_secret_access_key = ' + SecretAccessKey + '\n' + 'aws_session_token = ' + SessionToken)
fh.close()

