#define special aggregation function
def max_min(x):
    return x.max() - x.min()
max_min.__name__ = 'range' 

#create a grouped dataframe
df_grouped = pd.DataFrame(df.groupby('column 1', 'column 2')\
                          .agg({
                            'column 2' : 'count',
                            'column 3' : ['mean', 'std', max_min], 
                            'column 4' : ['sum', 'std'] })\
                          .reset_index())

#convert multilevel index (column names) to one level
df_grouped.columns = [' '.join(col).strip() for col in df_grouped.columns.values]

### Simple version ###
df_grouped = pd.DataFrame(df.groupby('column 1', 'column 2')['column 3'].sum().reset_index(name='column 3 aggr'))
