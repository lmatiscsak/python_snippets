#function
def myfunction(input1, input2):
    if input1 > input2:
        output = input1*2
    return output

#lambda function
x = lambda input1, input2: input1 * input2
print(x(2,3))

#ordinary function with lambda
def myfunc(n):
  return lambda a : a * n

mydoubler = myfunc(2)

print(mydoubler(11))


#lambda function in pandas dataframe
df['col'] = df.apply(lambda x: 'something' if pd.isnull(x['col']) else x['col'], axis=1)\
                    .apply(lambda x: x.capitalize() if x.upper() in value_list else 'something')