from datetime import datetime, timedelta

#time delta
date_predicate = str(datetime.now() - timedelta(days=120))[0:10]
print(date_predicate)

#string to date
date_str = '2020-09-22'
new_date = datetime.strptime(date_str, '%Y-%m-%d')


#pandas convert string to date
df['col1'] = df['col1'].replace(regex="/", value="-").str.slice(stop=10)
df['col1'] = pd.to_datetime(df['col1'], errors='coerce')

#pandas last day of the previous month
df['report_date_previous'] = pd.to_datetime(
        (pd.DatetimeIndex(df.report_date).year*10000 + pd.DatetimeIndex(df_tr.report_date).month*100 + 1).astype(str),
        format='%Y%m%d') \
        - timedelta(days=1)
