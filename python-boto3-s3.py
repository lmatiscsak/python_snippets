import boto3

s3 = boto3.resource('s3')
region = 'eu-west-1'
source_bucket_name = 'my-source-bucket'
target_bucket_name = 'my-target-bucket'

source_bucket = s3.Bucket(source_bucket_name)
target_bucket = s3.Bucket(target_bucket_name)

files_list = []
#iterating through relevant bucket objects
for obj in source_bucket.objects.filter(Prefix='folder/subfolder/'):
    source_file = str(obj.key)
	target_file = 'copied_' + source_file

	copy_source = {'Bucket': source_bucket_name, 'Key': source_file}
    target_bucket.copy(copy_source, target_file)

source_bucket.objects.filter(Prefix='my-prefix').delete()
	