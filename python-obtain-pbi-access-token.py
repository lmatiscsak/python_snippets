### With Adal Library ###
import adal

authority_url = 'https://login.windows.net/common'
resource_url = 'https://analysis.windows.net/powerbi/api'
client_id = 'cbad955f-ca18-40db-8b1c-9dd09425ffbc'
username = 'username@domain.com'
password = ''

context = adal.AuthenticationContext(authority=authority_url,
                                     validate_authority=True,
                                     api_version=None)

token = context.acquire_token_with_username_password(resource=resource_url,
                                                     client_id=client_id,
                                                     username=username,
                                                     password=password)


### With Requests Library ###
import requests
url = 'https://login.microsoftonline.com/common/oauth2/token'
data = {
        'grant_type': 'password',
        'scope': 'https://api.powerbi.com',
        'resource': 'https://analysis.windows.net/powerbi/api',
        'client_id': 'cbad955f-ca18-40db-8b1c-9dd09425ffbc',
        'username': 'username@domain.com',
        'password': ''
       }
r = requests.post(url, data=data)
access_token = r.json().get('access_token')