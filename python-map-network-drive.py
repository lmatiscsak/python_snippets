from subprocess import call

letter = 'm'
location = '\\MyNetworkDrive\Folder'
user = ''
password = ''

# Disconnect anything on M
call(r'net use ' + letter + ': /del', shell=True)

# Connect to shared drive, use drive letter M
call('net use ' + letter + ': ' + '\\' + location + ' /user:' +  user + ' ' + password, shell=True)