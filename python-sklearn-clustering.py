## AGGLOMERATIVE CLUSTERING ##

#creating a dendogram
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as plt
dendrogram = sch.dendrogram(sch.linkage(x_scaled, method='ward'))
plt.title('Dendrogram')
plt.xlabel('Activities')
plt.ylabel('Euclidean distances')
plt.show()

#agglomerative clustering
from sklearn.cluster import AgglomerativeClustering
hc = AgglomerativeClustering(n_clusters = 5, affinity = 'euclidean', linkage ='ward')
x_labels = hc.fit_predict(x_scaled)

#Adding labels back to the original dataset
df_labels = pd.DataFrame(x_labels, columns=['clusters'])
df = pd.concat([df, df_labels], axis=1, sort=False)


## K-MEANS CLUSTERING ##

from sklearn.cluster import KMeans
from sklearn import metrics
from scipy.spatial.distance import cdist

#Distortion: It is calculated as the average of the squared distances from the cluster centers of the respective clusters. 
#Typically, the Euclidean distance metric is used.
#Inertia: It is the sum of squared distances of samples to their closest cluster center.

distortions = [] 
inertias = [] 
mapping1 = {} 
mapping2 = {} 
K = range(1,10)

for k in K: 
    #building and fitting the model 
    kmeanModel = KMeans(n_clusters=k).fit(x_scaled)   
      
    distortions.append(sum(np.min(cdist(x_scaled, kmeanModel.cluster_centers_, 'euclidean'),axis=1)) / x_scaled.shape[0]) 
    inertias.append(kmeanModel.inertia_) 
  
    mapping1[k] = sum(np.min(cdist(x_scaled, kmeanModel.cluster_centers_, 'euclidean'),axis=1)) / x_scaled.shape[0] 
    mapping2[k] = kmeanModel.inertia_ 

#Visualizing Elbow Curves
#creating elbow chart using distortion
plt.plot(K, distortions, 'bx-') 
plt.xlabel('Values of K') 
plt.ylabel('Distortion') 
plt.title('The Elbow Method using Distortion') 
plt.show()

#creating elbow chart using inertias
plt.plot(K, inertias, 'bx-') 
plt.xlabel('Values of K') 
plt.ylabel('Inertia') 
plt.title('The Elbow Method using Inertia') 
plt.show()

kmeanModel_final = KMeans(n_clusters=4).fit(x_scaled)
df_labels_kmeans = pd.DataFrame(kmeanModel_final.labels_, columns=['clusters kmeans'])
df = pd.concat([df, df_labels_kmeans], axis=1, sort=False)