from os import listdir, path, walk
from re import findall

source_loc  = r'C:\folder\subfolder'
file_prefix = 'File_name_starts'
file_suffix = '.json'

for root, dirs, files in walk(source_loc):
    for file in files:
        if len(findall('^' + file_prefix + '.*.' + file_suffix,file)) > 0:
            list_with_path.append(path.join(root,file))
            file_list.append(file)