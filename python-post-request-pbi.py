import requests
access_token = ''
dataset_key = ''
group_id = ''
refresh_url = 'https://api.powerbi.com/v1.0/myorg/groups/' + group_id + '/datasets/' + dataset_key + '/refreshes'
header = {'Authorization': f'Bearer {access_token}'} #access token is coming from "obtain-pbi_access-token"
r = requests.post(url=refresh_url, headers=header)
r.raise_for_status()