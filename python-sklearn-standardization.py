#STANDARDIZING VARIABLES
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler() #create a scaler instance
x = df[list(df.columns[1:])] #select the columns which need to be standardized
scaler.fit(x)  #calculating mean and var of the variables
#print(scaler.mean_, scaler.var_)
StandardScaler(copy=True, with_mean=True, with_std=True) #set scaler parameters
x_scaled = scaler.transform(x) #create standardized variables