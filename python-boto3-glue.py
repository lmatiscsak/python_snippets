### run glue job ####
file_list = []
str_filenames = ','.join(files_list)
#running etl job
myNewJobRun = glue.start_job_run(
                                JobName='cs_glue_dev_datalake_hr_demographic_data_etl',
                                Arguments = {
                                            '--region': 'eu-west-1',
                                            '--filename_info': str_filenames
                                            }
                                )
print(str_filenames)

## this goes into the glue job
args = getResolvedOptions(sys.argv, ['JOB_NAME', 'region', 'filename_info'])
job.init(args['JOB_NAME'], args)
region = args['region']
str_filenames = args['filename_info']
file_list = str_filenames.split(',')

### table partitions ###

#list partitions
response = glue.get_partitions(
    DatabaseName = database_name,
    TableName = 'table_name'
)
response = response.get('Partitions')

partiton_list = []
for v in response:
    partition = v.get('Values')
    partiton_list.append(partition)

#delete partitions
for p in partiton_list:
    part1 = str(p[0])
    part2 = str(p[1])
    part3 = str(p[2])
    #print(part1 +', ' + part2 +', ' + part3)
    
    remove_partition = glue.delete_partition(
            DatabaseName = 'database name' ,
            TableName = 'table name',
            PartitionValues = [part1, part2, part3]
            )

#add partitions to athena table
athena.start_query_execution(
                            QueryString='MSCK REPAIR TABLE ' + database_name + ' ' + table_name + ';',
                            QueryExecutionContext={'Database': 'database name'},
                            ResultConfiguration={
                                                'OutputLocation': 's3://my-bucket/query_output',
                                                'EncryptionConfiguration': {'EncryptionOption': 'SSE_S3'}
                                                }
                            )
			