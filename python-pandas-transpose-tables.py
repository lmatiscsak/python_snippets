import pandas as pd
df = pd.DataFrame()
for f in list_with_path:
    temp_df = pd.read_excel( f, sheet_name = sheet_name, nrows=3)
    temp_df = temp_df.transpose()
    temp_df = temp_df[1:]
    
    df = df.append(temp_df)
del temp_df
df = df.reset_index(drop=True)
df.columns = ['col name 1', 'col name 2', 'col name 3']