import io
import boto3
import pandas as pd
import s3fs

bucketname = 'mybucket'
filename = 'folder/subfolder/part-00000-89e7f66f-d170-424c-a118-a72ec7d840fb-c000.snappy.parquet'

s3_client = boto3.client('s3')
response = s3_client.get_object(Bucket = bucketname, Key = filename)
sourcefile = response["Body"].read()

df = pd.read_parquet(io.BytesIO(sourcefile), engine='pyarrow')

###Export df to parquet on S3###
s3_url = 's3://mybucket/folder/test.parquet'
df.to_parquet(s3_url)